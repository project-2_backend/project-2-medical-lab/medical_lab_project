<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('biomarkers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            //$table->char('value')->nullable();
            $table->char('measuring_unit');
            $table->foreignId('test_id')->constrained('tests')->cascadeOnDelete();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('biomarkers');
    }
};
