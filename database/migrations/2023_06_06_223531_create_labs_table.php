<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('labs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('address_id')->constrained('addresses')->cascadeOnDelete();
            $table->integer('phone_number');
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->text('certificate');
            //$table->float('rate')->default(0.0);
            $table->text('image');
            $table->text('stamp');
            $table->time('open_hour');
            $table->time('close_hour');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('labs');
    }
};
