<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('appointment__tests', function (Blueprint $table) {
           $table->id(); 
            $table->foreignId('appointments_id')->reference('id')->on('appointments')->OnDelete('cascade');
            $table->foreignId('tests_id')->reference('id')->on('tests')->OnDelete('cascade');
            $table->timestamps();
        });   
    }
    

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('appointment__tests');
    }
};
