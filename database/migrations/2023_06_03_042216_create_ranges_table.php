<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ranges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('biomarker_id')->constrained('biomarkers')->cascadeOnDelete();
            $table->char('max_value');
            $table->char('min_value');
            $table->integer('age_max')->nullable();
            $table->integer('age_min')->nullable();
            $table->char('gender');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ranges');
    }
};
