<?php

namespace App\Models;

use http\Env\Response;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Appointment extends Model
{
    use HasFactory;

    protected $table = 'appointments';
    public $fillable = [
        'users_id', 'labs_id', 'hour','date', 'total_price', 'tests_id', 'test__result_id'
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function tests(): BelongsToMany
    {
        return $this->belongsToMany(Test::class, 'appointment__tests', 'appointments_id', 'tests_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function lab(): BelongsTo
    {
        return $this->belongsTo(Lab::class, 'lab_id');
    }

    public function testResults(): HasMany
    {
        return $this->hasMany(TestResult::class, 'appointment_id');
    }

}
