<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LabCategory extends Model
{
    use HasFactory;

    protected $table = 'lab__category';
    public $fillable = [
        'category_id', 'labs_id',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';
}
