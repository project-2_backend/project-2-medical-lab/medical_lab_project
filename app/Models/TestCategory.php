<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TestCategory extends Model
{
    use HasFactory;

    protected $table = 'test_categories';
    public $fillable = [
        'name', 'description', 'image',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function tests(): HasMany
    {
        return $this->hasMany(Test::class, 'tests_id');
    }

    public function labs(): BelongsToMany
    {
        return $this->belongsToMany(Lab::class);
    }
}
