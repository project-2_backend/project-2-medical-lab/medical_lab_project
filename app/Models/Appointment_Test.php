<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment_Test extends Model
{
    use HasFactory;

    protected $table = 'appointment__tests';
    public $fillable = [
        'appointments_id', 'tests_id' => 'array', 'total_price',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

}
