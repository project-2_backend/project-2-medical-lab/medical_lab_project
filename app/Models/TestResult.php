<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TestResult extends Model
{
    use HasFactory;

    protected $table = 'test_results';
    public $fillable = [
        'appointment_id', 'status', 'ranges_id', 'biomarker_id', 'result', 'user_id', 'test_id', 'test_date'
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $casts = [
        'biomarker_id' => 'array',
        'result' => 'array',
    ];

    public function appointment(): BelongsTo
    {
        return $this->belongsTo(Appointment_Test::class, 'appointment_test_id');
    }

    public function range(): BelongsTo
    {
        return $this->belongsTo(Range::class, 'ranges_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function biomarker(): BelongsTo
    {
        return $this->belongsTo(Biomarker::class);
    }

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class, 'test_id');
    }
}
