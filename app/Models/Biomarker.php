<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\hasOne;

class Biomarker extends Model
{
    use HasFactory;


    protected $table = 'biomarkers';
    public $fillable = [
        'name', 'description', 'value', 'measuring_unit', 'test_id',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class, 'test_id');
    }

    public function ranges(): HasMany
    {
        return $this->hasMany(Range::class, 'biomarker_id');
    }

    public function results(): HasMany
    {
        return $this->hasMany(TestResult::class, 'test_result_id');
    }
}
