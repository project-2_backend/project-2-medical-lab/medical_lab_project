<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Lab extends Model
{
    use HasFactory;

    protected $table = 'labs';
    public $fillable = [
        'name', 'address_id', 'phone_number', 'certificate', 'rate', 'image', 'open_hours', 'close_hour',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function tests(): BelongsToMany
    {
        return $this->belongsToMany(Test::class);
    }

    public function appointments(): HasMany
    {
        return $this->hasMany(Appointment::class, 'appointments_id');
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function rates(): HasMany
    {
        return $this->hasMany(Rate::class, 'lab_id');
    }

}
