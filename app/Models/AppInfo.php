<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppInfo extends Model
{
    use HasFactory;

    protected $table = 'app_infos';
    public $fillable = [
        'name', 'logo', 'phone_number', 'email',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';
}
