<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Range extends Model
{
    use HasFactory;

    protected $table = 'ranges';
    public $fillable = [
        'biomarker_id', 'max_value', 'min_value', 'measuring_unit', 'age_max','age_min', 'gender',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function biomarker(): BelongsTo
    {
        return $this->belongsTo(Biomarker::class, 'biomarker_id');
    }
    public function testResults(): HasMany
    {
        return $this->hasMany(TestResult::class, 'test__results_id');
    }
}
