<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Test extends Model
{
    use HasFactory;

    protected $table = 'tests';
    public $fillable = [
        'name', 'description', 'price', 'image', 'test_category_id',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function test_category(): BelongsTo
    {
        return $this->belongsTo(TestCategory::class, 'test_category_id');
    }

    public function biomarkers(): HasMany
    {
        return $this->hasMany(Biomarker::class, 'biomarker_id');
    }

    public function labs(): BelongsToMany
    {
        return $this->belongsToMany(Lab::class);
    }

    public function appointments(): BelongsToMany
    {
        return $this->belongsToMany(Appointment::class, 'appointment__tests', 'tests_id', 'appointments_id');
    }
    public function results(): HasMany
    {
        return $this->hasMany(TestResult::class, 'test_result_id');
    }
}
