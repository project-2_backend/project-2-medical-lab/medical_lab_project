<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model
{
    use HasFactory;

    protected $table = 'cities';
    public $fillable = [
        'title',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class, 'address_id');
    }
}
