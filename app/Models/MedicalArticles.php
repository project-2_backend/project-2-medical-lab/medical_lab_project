<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalArticles extends Model
{
    use HasFactory;

    protected $table = 'medical_articles';
    public $fillable = [
        'title', 'description', 'image',
    ];
    public $timestamps = false;
    protected $primaryKey = 'id';
}
