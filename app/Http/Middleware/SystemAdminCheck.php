<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;

class SystemAdminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user_id = $request->user('sanctum')->id;
        $user_u = DB::table('users')->where('id', $user_id)->value('account_type');
        //dd($user_id);
        if ($user_u != 2) {
            return response()->json(['message' => 'You are not Authorized!(SYSTEM ADMIN)'], \Illuminate\Http\Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
