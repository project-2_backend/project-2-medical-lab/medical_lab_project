<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class LabManCheck
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user_id = $request->user('sanctum')->id;
        //dd($user_id);
        $user_u = DB::table('users')->where('id', $user_id)->value('account_type');
        if ($user_u != 1) {
            return response()->json(['message' => 'You are not Authorized! (LAB MANAGER)'], \Illuminate\Http\Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
