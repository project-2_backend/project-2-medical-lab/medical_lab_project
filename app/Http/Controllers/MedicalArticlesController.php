<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\MedicalArticles;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class MedicalArticlesController extends Controller
{
    public function index()
    {
        $medicalArticles = MedicalArticles::all();
        return response()->json($medicalArticles, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|mimes:jpeg,jpg,png|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $medicalArticle = new MedicalArticles();
        $medicalArticle->title = $request->title;
        $medicalArticle->description = $request->description;
        $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
        $request->image->move('uploads/medicalArticles', $file_name);
        $medicalArticle->image = '/' . 'uploads/medicalArticles' . '/' . $file_name;
        $medicalArticle->save();
        return response()->json($medicalArticle, Response::HTTP_OK);
    }

    public function show(Request $request)
    {
        $id = $request->query('ma_id');
        $medicalArticle = MedicalArticles::find($id);
        if (!$medicalArticle) {
            return response()->json(['message' => 'Medical Articles not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($medicalArticle, Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        $id = $request->query('ma_id');
        $medicalArticle = MedicalArticles::find($id);
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string ', Rule::unique('medical_articles', 'title')],
            'description' => ['required', 'string '],
            'image' => ['sometimes', ' mimes:jpeg,jpg,png ', ' max:2048'],
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $compare = MedicalArticles::where('title', '=', $request->title)
            ->get();
        if (!$compare->isEmpty()) {
            return response()->json(['message' => 'This medical article is already exist !'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $medicalArticle->title = $request->title;
            $medicalArticle->description = $request->description;
            $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move('uploads/medicalArticles', $file_name);
            $medicalArticle->image = '/' . 'uploads/medicalArticles' . '/' . $file_name;
            $medicalArticle->save();
            return response()->json('Updated Successfully!', Response::HTTP_OK);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->query('ma_id');
        $medicalArticle = MedicalArticles::find($id);
        $medicalArticle->delete();
        return response()->json(['message' => 'Medical Articles deleted successfully'], Response::HTTP_OK);
    }
}
