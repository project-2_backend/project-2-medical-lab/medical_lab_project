<?php

namespace App\Http\Controllers\Address;

use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;


class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $addresses = Address::query()->get();
        return response()->json($addresses, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [/**Validate the inputs**/
            'title' => ['required', 'string'],
            'street' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $title = $request->title;
        $city_id = $request->city_id;
        $street = $request->street;
        $address = Address::query()->create([/** Creating the address **/
            'title' => $title,
            'city_id' => $city_id,
            'street' => $street,
        ]);
        return response()->json($address, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        $id = $request->query('address_id');
        $address = Address::query()->find($id);
        if (is_null($address)) {
            return response()->json(['Address not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json(['Address details' => $address], Response::HTTP_OK);
    }

    public function search(Request $request)
    {
        $search_text = $request->search_text;
        //dd($search_text);
        $address = Address::query()
            ->where("title", "LIKE", "%" . $search_text . "%")
            ->orWhere("street", "LIKE", "%" . $search_text . "%")
            ->get();
        return response()->json($address, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $id = $request->query('address_id');
        $address = Address::query()->find($id);
        $validator = Validator::make($request->all(), [/**Validate the inputs**/
            'title' => ['required', 'string'],
            'street' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $title = $request->title;
        $city_id = $request->city_id;
        $street = $request->street;
        $address->update([/** Editing the address **/
            'title' => $title,
            'city_id' => $city_id,
            'street' => $street,
        ]);
        return response()->json($address, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $id = $request->query('address_id');
        $address = Address::query()->find($id);
        $address->delete();
        return response()->json(['Address has been deleted'], Response::HTTP_OK);
    }
}
