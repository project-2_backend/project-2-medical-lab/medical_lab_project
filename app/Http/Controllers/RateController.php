<?php

namespace App\Http\Controllers;

use App\Models\Lab;
use App\Models\Rate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RateController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [/**Validate the inputs**/
            'rate' => ['required', 'integer', 'min:0', 'max:10'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $lab_id = $request->lab_id;
        $exist = Rate::query()
            ->where('user_id', $request->user('sanctum')->id)
            ->where('lab_id', $lab_id)
            ->exists();
        if ($exist) {
            Rate::query()->where('user_id', $request->user('sanctum')->id)
                ->where('lab_id', $lab_id)
                ->where('rate');
            return response()->json(['You already rated!'], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        Rate::query()->create([/** Creating like **/
            'rate' => $request->rate,
            'lab_id' => $lab_id,
            'user_id' => $request->user('sanctum')->id
        ]);
        $lab = Lab::query()->find($lab_id);
        $rates = $lab->rates()->get();
        $rates_avg = $rates->avg('rate');
        return response()->json(['Message' => 'Your rate has been added!', 'Lab rate:' => $rates_avg], Response::HTTP_OK);

    }

    public function show(Request $request)
    {
        $lab_id = $request->query('lab_id');
        $lab = Lab::query()->find($lab_id);
        $rates = $lab->rates()->get();
        $rates_avg = $rates->avg('rate');
        return response()->json(['Rate : '=>$rates_avg], Response::HTTP_OK);
    }

    public function update(Request $request, Rate $rate)
    {
        $validator = Validator::make($request->all(), [/**Validate the inputs**/
            'rate' => ['required', 'integer', 'min:0', 'max:10'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $lab_id = $request->lab_id;
        $exist = Rate::query()
            ->where('user_id', $request->user('sanctum')->id)
            ->where('lab_id', $lab_id)
            ->exists();
        if ($exist) {
            $rate_ob = DB::table('rates')->where('user_id', $request->user('sanctum')->id)
                ->where('lab_id', $lab_id)->first();
            // dd($rate->id);
            Rate::query()->find($rate_ob->id)->update([
                'rate' => $request->rate,
            ]);
            $lab = Lab::query()->find($lab_id);
            $rates = $lab->rates()->get();
            $rates_avg = $rates->avg('rate');
            return response()->json(['Your rate has been updated!' => $rates_avg], Response::HTTP_OK);
        }
        return response()->json(['Error'], Response::HTTP_BAD_REQUEST);
    }

    public function destroy(Request $request)
    {
        $lab_id = $request->lab_id;
        $exist = Rate::query()
            ->where('user_id', $request->user('sanctum')->id)
            ->where('lab_id', $lab_id)
            ->exists();
        if ($exist) {
            Rate::query()->where('user_id', $request->user('sanctum')->id)
                ->where('lab_id', $lab_id)
                ->delete();
            $lab = Lab::query()->find($lab_id);
            $rates = $lab->rates()->get();
            $rates_avg = $rates->avg('rate');
            return response()->json(['Message' => 'Your rate has been removed!', 'Lab rate:' => $rates_avg], Response::HTTP_OK);
        } else {
            return response()->json(['Error'], Response::HTTP_BAD_REQUEST);
        }
    }
}
