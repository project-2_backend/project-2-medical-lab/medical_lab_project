<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\AppInfo;
use Illuminate\Support\Facades\Validator;

class AppInfoController extends Controller
{

    public function show_appInfo()
    {
        $appInfo = AppInfo::all();
        return response()->json($appInfo);
    }

    public function add_appInfo(Request $request)
    {
        $appInfo = AppInfo::first();
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'logo' => 'required|mimes:jpeg,jpg,png|max:2048',
            'phone_number' => 'required|digits:10',
            'email' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        if ($appInfo) {
            return response()->json(['message' => 'Only one record is allowed'], 422);
        }

        $appInfo = new AppInfo;
        $appInfo->name = $request->name;
        $file_name = rand() . time() . '.' . $request->logo->getClientOriginalExtension();
        $request->image->move('uploads/appInfo', $file_name);
        $appInfo->image = '/' . 'uploads/appInfo' . '/' . $file_name;
        $appInfo->phone_number = $request->phone_number;
        $appInfo->email = $request->email;

        $appInfo->save();

        return response()->json(['message' => 'App Info created successfully'], 201);
    }

    public function edit_appInfo(Request $request, $id)
    {
        $appInfo = AppInfo::find($id);
        /**  $validator = Validator::make($request->all(), [
         * 'name' => 'required|string',
         * 'logo' => 'required|mimes:jpeg,jpg,png|max:2048',
         * 'phone_number' => 'required|digits:10',
         * 'email' => 'required|string',
         * ]);
         *
         * if ($validator->fails()) {
         * return response()->json(['errors' => $validator->errors()], 422);
         * }**/

        // $appInfo = AppInfo::find($id);
        if (!$appInfo) {
            return response()->json(['message' => 'Record not found'], 404);
        }
        if (isset($appInfo->name)) {
            $appInfo->name = $request->name;
        }
        if (isset($file_name)) {
            $file_name = rand() . time() . '.' . $request->logo->getClientOriginalExtension();
            $request->image->move('uploads/appInfo', $file_name);
            $appInfo->image = '/' . 'uploads/appInfo' . '/' . $file_name;
        }
        if (isset($appInfo->phone_number)) {
            $appInfo->phone_number = $request->phone_number;
        }
        if (isset($appInfo->email)) {
            $appInfo->email = $request->email;
        }
        $appInfo->save();
        return response()->json($appInfo);
    }
}
