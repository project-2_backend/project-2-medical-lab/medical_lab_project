<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TestCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Test;
use App\Models\Lab;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use function Symfony\Component\Translation\t;

class TestController extends Controller
{


    public function index()
    {
        $test = Test::all();
        return response()->json($test, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string ', Rule::unique('tests', 'name')],
            'description' => ['required', 'string '],
            'price' => ['required ', ' integer'],
            'image' => ['required ', ' mimes:jpeg,jpg,png ', ' max:2048'],
            'test_category_id' => ['required ', Rule::exists('test_categories', 'id')],
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $test = new Test();
        $test->name = $request->name;
        $test->description = $request->description;
        $test->price = $request->price;
        $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
        $request->image->move('uploads/tests', $file_name);
        $test->image = '/' . 'uploads/tests' . '/' . $file_name;
        $test->test_category_id = $request->test_category_id;
        $test->save();
        $test->labs()->attach($request->labs);
        return response()->json($test, Response::HTTP_OK);
    }
    public function show_test_by_CategoryId($catId)
    {
        $tests = Test::where('test_category_id', $catId)->get();
        if (!$tests) {
            return response()->json(['message' => 'Category not found'], 404);
        }

        return response()->json($tests, 200);
    }

    public function show(Request $request)
    {
        $id = $request->query('test_id');
        $test = Test::find($id);
        if (!$test) {
            return response()->json(['message' => 'Test not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($test, Response::HTTP_OK);
    }

    //not ready yet !!!
    public function lab_tests(Request $request)
    {
        $lab_id = $request->query('lab_id');
        $cat_id = $request->query('cat_id');
        $lab = Lab::query()->find($lab_id);
        $lab_tests = $lab->tests()->where('test_category_id', $cat_id)->get();
        dd($lab_tests);

        // dd($lab_tests->tests());
        if (!$lab_tests) {
            return response()->json(['message' => 'Lab does not have tests'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($lab_tests, Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        $id = $request->query('test_id');
        $test = Test::find($id);
        $validator = Validator::make($request->all(), [
            'name' => ['sometimes', 'string ', Rule::unique('tests', 'name')],
            'description' => ['sometimes', 'string '],
            'price' => ['sometimes', ' integer'],
            'image' => ['sometimes', ' mimes:jpeg,jpg,png ', ' max:2048'],
            'test_category_id' => ['sometimes', Rule::exists('test_categories', 'id')],
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $compare = Test::where('name', '=', $request->name)
            ->get();
        if (!$compare->isEmpty()) {
            return response()->json(['message' => 'This Test is already exist !'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move('uploads/tests', $file_name);
            $test->image = '/' . 'uploads/tests' . '/' . $file_name;
            $test->update($request->all());
            return response()->json('Updated Successfully!', Response::HTTP_OK);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->query('test_id');
        $test = Test::findOrFail($id);
        $test->delete();
        return response()->json(['message' => 'Test deleted successfully'], Response::HTTP_OK);
    }
}
