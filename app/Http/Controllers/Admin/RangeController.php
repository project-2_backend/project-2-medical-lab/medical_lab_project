<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Range;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RangeController extends Controller
{


    public function index()
    {
        $range = Range::all();
        return response()->json($range, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'biomarker_id' => ['required', Rule::exists('biomarkers', 'id')],
            'max_value' => ['required'],
            'min_value' => ['required'],
            'age_max' => ['required', 'integer'],
            'age_min' => ['required', 'integer'],
            'gender' => ['required', 'string', 'max:6'],
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $range = new Range();
        $range->biomarker_id = $request->biomarker_id;
        $range->max_value = $request->max_value;
        $range->min_value = $request->min_value;
        $range->age_max = $request->age_max;
        $range->age_min = $request->age_min;
        $range->gender = $request->gender;
        $range->save();
        return response()->json($range, Response::HTTP_OK);
    }

    public function show(Request $request)
    {
        $id = $request->query('range_id');
        $range = Range::find($id);
        if (!$range) {
            return response()->json(['message' => 'Range not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($range, Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        $id = $request->query('range_id');
        $range = Range::find($id);
        $validator = Validator::make($request->all(), [
            'biomarker_id' => ['sometimes', Rule::exists('biomarkers', 'id')],
            'max_value' => ['sometimes'],
            'min_value' => ['sometimes'],
            'measuring_unit' => ['sometimes', 'string'],
            'age' => ['sometimes', 'integer'],
            'gender' => ['sometimes', 'string', 'max:6'],
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $compare = Range::where('biomarker_id', '=', $request->biomarker_id)
            ->get();
        if (!$compare->isEmpty()) {
            return response()->json(['message' => 'This Range is already exist !'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $range->update($request->all());
            return response()->json('Updated Successfully!', Response::HTTP_OK);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->query('range_id');
        $range = Range::find($id);
        $range->delete();
        return response()->json(['message' => 'Range deleted successfully'], Response::HTTP_OK);
    }
}
