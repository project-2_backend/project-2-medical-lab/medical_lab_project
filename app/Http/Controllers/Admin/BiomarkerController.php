<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TestResultController;
use App\Models\TestResult;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Biomarker;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PhpParser\Node\Expr\List_;

class BiomarkerController extends Controller
{


    public function index()
    {
        $biomarker = Biomarker::all();
        return response()->json($biomarker, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', ' string', Rule::unique('biomarkers', 'name')],
            'description' => ['required', 'string'],
            'measuring_unit' => ['required', 'string'],
            'test_id' => ['required', Rule::exists('tests', 'id')],
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $biomarker = new Biomarker();
        $biomarker->name = $request->name;
        $biomarker->description = $request->description;
        $biomarker->measuring_unit = $request->measuring_unit;
        $biomarker->test_id = $request->test_id;
        $biomarker->save();
        return response()->json($biomarker, Response::HTTP_OK);
    }

    public function show(Request $request)
    {
        $id = $request->query('biomarker_id');
        $biomarker = Biomarker::find($id);
        if (!$biomarker) {
            return response()->json(['message' => 'Biomarker not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($biomarker, Response::HTTP_OK);
    }

    public function show_biomarker_by_TestId($testId)
    {
        $biomarkers = Biomarker::where('test_id', $testId)->get();
        if (!$biomarkers) {
            return response()->json(['message' => 'Biomarker not found'], 404);
        }

        return response()->json($biomarkers, 200);
    }


    public function update(Request $request)
    {
        $id = $request->query('biomarker_id');
        $biomarker = Biomarker::find($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'test_id' => ['required', Rule::exists('tests', 'id')],
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        //dd($request->name);
        $compare = Biomarker::where('name', '=', $request->name)
            ->get();
        if (!$compare->isEmpty()) {
            return response()->json(['message' => 'This biomarker is already exist !'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $biomarker->name = $request->name;
            $biomarker->description = $request->description;
            $biomarker->test_id = $request->test_id;
            $biomarker->save();
            return response()->json('Updated Successfully!', Response::HTTP_OK);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->query('biomarker_id');
        $biomarker = Biomarker::find($id);
        $biomarker->delete();
        return response()->json(['message' => 'Biomarker deleted successfully'], Response::HTTP_OK);
    }

    public function statistics_pos(Request $request)
    {
        $biomarker_id = $request->biomarker_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $pos = TestResult::where('biomarker_id', $biomarker_id)
            ->whereBetween('test_date', [$start_date, $end_date])
            ->where('status', 'positive')
            ->get();
        $all = TestResult::where('biomarker_id', $biomarker_id)
            ->whereBetween('test_date', [$start_date, $end_date])
            ->get();

        $pos_count = $pos->count();
        $all_count = $all->count();
        $percentage = ($pos_count / $all_count) * 100;

        return response()->json([['percentage:', $percentage], ['all testers:', $all_count],
            ['low:', $pos_count]], Response::HTTP_OK);
    }

    public function statistics_low_bio(Request $request)
    {
        $biomarker_id = $request->biomarker_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $lows = TestResult::where('biomarker_id', $biomarker_id)
            ->whereBetween('test_date', [$start_date, $end_date])
            ->where('status', 'Low!')
            ->get();
        $all = TestResult::where('biomarker_id', $biomarker_id)
            ->whereBetween('test_date', [$start_date, $end_date])
            ->get();

        $lows_count = $lows->count();
        $all_count = $all->count();
        $percentage = ($lows_count / $all_count) * 100;

        return response()->json([['percentage:', $percentage], ['all testers:', $all_count], ['low:', $lows_count]], Response::HTTP_OK);
    }
}
