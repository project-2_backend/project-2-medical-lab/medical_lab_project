<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lab;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\TestCategory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TestCategoryController extends Controller
{
    public function index()
    {
        $categoriesTest = TestCategory::all();
        return response()->json($categoriesTest, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string ', Rule::unique('test_categories', 'name')],
            'description' => ['required', 'string '],
            'image' => ['required', ' mimes:jpeg,jpg,png ', ' max:2048'],
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $categoriesTest = new TestCategory();
        $categoriesTest->name = $request->name;
        $categoriesTest->description = $request->description;
        $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
        $request->image->move('uploads/testCategory', $file_name);
        $categoriesTest->image = '/' . 'uploads/testCategory' . '/' . $file_name;
        //$categoriesTest->image = $request->image;
        $categoriesTest->save();
        return response()->json($categoriesTest, Response::HTTP_OK);
    }


    public function show(Request $request)
    {
        $id = $request->query('test_category_id');
        $categoriesTest = TestCategory::query()->find($id);
        if (!$categoriesTest) {
            return response()->json(['message' => 'Test Category not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($categoriesTest, Response::HTTP_OK);
    }


    //not ready yet !!!
    public function test_category_lab(Request $request)
    {
        $id = $request->query('lab_id');
        $lab = Lab::query()->find($id);
        $lab_categories = $lab->categories()->get();

        //dd($lab_tests);
        if (!$lab_categories) {
            return response()->json(['message' => 'Lab does not have categories'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($lab_categories, Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        $id = $request->query('test_category_id');
        $categoriesTest = TestCategory::query()->findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string ', Rule::unique('test_categories', 'name')],
            'description' => ['required', 'string '],
            'image' => ['sometimes', ' mimes:jpeg,jpg,png ', ' max:2048'],
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $compare = TestCategory::where('name', '=', $request->name)
            ->get();
        if (!$compare->isEmpty()) {
            return response()->json(['message' => 'This Category is already exist !'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $categoriesTest->name = $request->name;
            $categoriesTest->description = $request->description;
            $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move('uploads/testCategory', $file_name);
            $categoriesTest->image = '/' . 'uploads/testCategory' . '/' . $file_name;
            $categoriesTest->save();
            return response()->json($categoriesTest, Response::HTTP_OK);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->query('test_category_id');
        $categoryTest = TestCategory::find($id);
        $categoryTest->delete();
        return response()->json(['message' => 'Test category deleted successfully'], Response::HTTP_OK);
    }
}
