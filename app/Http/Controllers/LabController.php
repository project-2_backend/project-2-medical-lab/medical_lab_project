<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\City;
use App\Models\Rate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Lab;
use App\Models\Test;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LabController extends Controller
{
    public function index()
    {
        $lab = Lab::all();
        return response()->json($lab, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'address_id' => 'required|exists:addresses,id',
            'phone_number' => 'required|digits:10',
            'certificate' => 'required | string',
            'image' => 'required|mimes:jpeg,jpg,png|max:2048',
            'stamp' => 'required|mimes:jpeg,jpg,png|max:2048',
            'work_hours' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $lab = new Lab();
        $lab->name = $request->name;
        $lab->address_id = $request->address_id;
        $lab->phone_number = $request->phone_number;
        $lab->certificate = $request->certificate;
        $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
        $request->image->move('uploads/labs', $file_name);
        $lab->image = '/' . 'uploads/labs' . '/' . $file_name;
        $file_name_stamp = rand() . time() . '.' . $request->stamp->getClientOriginalExtension();
        $request->stamp->move('uploads/stamps', $file_name_stamp);
        $lab->stamp = '/' . 'uploads/stamps' . '/' . $file_name_stamp;
        $lab->work_hours = $request->work_hours;
        $lab->save();
        $lab->tests()->attach($request->tests);
        return response()->json($lab, Response::HTTP_OK);
    }

    public function show(Request $request)
    {
        $id = $request->query('lab_id');
        $lab = Lab::find($id);
        if (!$lab) {
            return response()->json(['message' => 'Lab not found'], Response::HTTP_NOT_FOUND);
        }
        return response()->json($lab, Response::HTTP_OK);
    }



    public function nearby_labs(Request $request)
    {
        $user_id = $request->user('sanctum')->id;
        $user_city_id = Address::find(User::find($user_id)->address_id)->city_id;
        $labs = Lab::all();
        $nearby_labs = [];
        foreach ($labs as $lab) {
            $lab_city_id = Address::find($lab->address_id)->city_id;
            if ($lab_city_id == $user_city_id) {
                $nearby_labs [] = $lab;
            }
        }
        return response()->json($nearby_labs, Response::HTTP_OK);
    }

    public function top_rated_labs(Request $request)
    {
        $topTenRatedLabs = Lab::with('rates')
            ->get()
            ->map(function ($lab) {
                $lab->averageRate = $lab->rates->avg('rate');
                return $lab;
            })
            ->sortByDesc('averageRate')
            ->take(10);
        return response()->json($topTenRatedLabs, Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        $id = $request->query('lab_id');
        $lab = Lab::find($id);
        $validator = Validator::make($request->all(), [
            'name' => ['sometimes', 'string'],
            'address_id' => ['sometimes', Rule::exists('addresses', 'id')],
            'phone_number' => ['required', 'integer'],
            'certificate' => ['required ', 'string'],
            'image' => ['sometimes', ' mimes:jpeg,jpg,png ', ' max:2048'],
            'stamp' => 'required|mimes:jpeg,jpg,png|max:2048',
            'work_hours' => ['sometimes', 'string'],
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $compare = Lab::where('name', '=', $request->name)
            ->get();
        if (!$compare->isEmpty()) {
            return response()->json(['message' => 'This Lab is already exist !'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $lab->name = $request->name;
            $lab->address_id = $request->address_id;
            $lab->phone_number = $request->phone_number;
            $lab->certificate = $request->certificate;
            $file_name = rand() . time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move('uploads/labs', $file_name);
            $lab->image = '/' . 'uploads/labs' . '/' . $file_name;
            $file_name_stamp = rand() . time() . '.' . $request->stamp->getClientOriginalExtension();
            $request->stamp->move('uploads/stamps', $file_name_stamp);
            $lab->stamp = '/' . 'uploads/stamps' . '/' . $file_name_stamp;
            $lab->work_hours = $request->work_hours;
            $lab->save();
            return response()->json('Updated Successfully!', Response::HTTP_OK);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->query('lab_id');
        $lab = Lab::find($id);
        $lab->delete();
        return response()->json(['message' => 'Lab deleted successfully'], Response::HTTP_OK);
    }
}
