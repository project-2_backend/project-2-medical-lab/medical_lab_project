<?php

namespace App\Http\Controllers;

use App\Models\TestResult;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Appointment;
use App\Models\Lab;
use App\Http\Controllers\Auth;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    public function index()
    {
        $appointment = Appointment::all();
        return response()->json($appointment, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lab_id' => 'required|exists:labs,id',
            'date' => 'required|date',
            //'hour' => 'required|date',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $appointment = new Appointment();
        $appointment->user_id = $request->user('sanctum')->id;
        $appointment->lab_id = $request->lab_id;
        $appointment->date = $request->date;
        $appointment['hour'] = Carbon::createFromFormat('g:i A', $request->input('hour'))->format('H:i');
        $lab = Lab::find($appointment->lab_id);
      if ($appointment->hour->lt($lab->open_hour) || $appointment->hour->gt($lab->close_hour)) {
        return response()->json(['message' => 'The test you entered dons`t contain this biomarker ']);
       }
        $appointment->save();
        $appointment->tests()->attach($request->tests_id);
        return response()->json($appointment, Response::HTTP_OK);
    }

    public function show_appointment_by_UserId(Request $request)
    {
        $appointments = Appointment::where('user_id', $request->user_id)->get();
        if (!$appointments) {
            return response()->json(['message' => 'Appointment not found'], 404);
        }
        return response()->json($appointments, 200);
    }

    public function show_appointment_by_LabId($labId)
    {
        $appointments = Appointment::where('lab_id', $labId)->get();
        if (!$appointments) {
            return response()->json(['message' => 'Appointment not found'], 404);
        }
        return response()->json($appointments, 200);
    }

    public function done_appointments(Request $request)
    {
        $res = Appointment::whereHas('testResults')
            //->where('lab_id',$lab_id)
            ->get();
        return response()->json($res, 200);
    }

    public function pending_appointments(Request $request)
    {
        $pending_app = Appointment::whereDoesntHave('testResults')
            //->where('lab_id',$lab_id)
            ->get();
        $tests = $pending_app->tests()->get();
        //dd($pending_app);
        return response()->json($pending_app, 200);
    }

    public function destroy(Request $request)
    {
        $id = $request->query('appointment_id');
        $appointment = Appointment::find($id);
        $appointment->delete();
        return response()->json(['message' => 'Appointment deleted successfully'], Response::HTTP_OK);
    }
}
