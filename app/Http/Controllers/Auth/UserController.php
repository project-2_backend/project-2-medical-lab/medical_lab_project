<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class UserController extends Controller
{
    public function register(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'digits:10', Rule::unique('users', 'phone_number')],
            'email' => ['required', 'email', 'string', 'max:255', Rule::unique('users', 'email')],
            'password' => ['required', 'string', 'min:8'],
            'gender' => ['required', 'string', 'min:4', 'max:6'],
            'birth_date' => ['required', 'date'],
            'account_type' => ['required', 'integer', 'min:0', 'max:3'],
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        //create new user
        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone_number = $request->phone_number;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->birth_date = Carbon::parse($request->birth_date);
        $user->age = $user->birth_date->diffInYears(Carbon::now());
        $user->address_id = $request->address_id;
        $user->account_type = $request->account_type;
        $user->save();
        $token = $user->createToken("auth_Token")->plainTextToken;
        //$user_u = DB::table('users')->where('email', $user->email)->value('is_admin');
        if ($user->account_type == 0) {
            return response()->json(['Account type:' => 'Regular User', 'User' => $user, 'token' => $token], Response::HTTP_OK);
        } elseif ($user->account_type == 1) {
            return response()->json(['Account type:' => 'Lab Manager', 'User' => $user, 'token' => $token], Response::HTTP_OK);
        } elseif ($user->account_type == 2) {
            return response()->json(['Account type:' => 'System Admin', 'User' => $user, 'token' => $token], Response::HTTP_OK);
        }
        return response()->json(['Something went Wrong!'], Response::HTTP_UNPROCESSABLE_ENTITY);
        // $user->assignRole('User');
    }

    public function login(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
            //'phone_number' => ['sometimes', 'required', 'integer'],
            'email' => ['required', 'email', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = (new \App\Models\User)->where('email', '=', $request->email)->first();
        if (isset($user->email)) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('auth_Token')->plainTextToken;
                $account_type = DB::table('users')->where('email', $user->email)->value('account_type');
                //dd($account_type);
                if ($account_type == 0) {
                    return response()->json(['Account type:' => 'Regular User', 'User' => $user, 'token' => $token], Response::HTTP_OK);
                } elseif ($account_type == 1) {
                    return response()->json(['Account type:' => 'Lab Manager', 'User' => $user, 'token' => $token], Response::HTTP_OK);
                } elseif ($account_type == 2) {
                    return response()->json(['Account type:' => 'System Admin', 'User' => $user, 'token' => $token], Response::HTTP_OK);
                }
                //return response()->json(['Account type:' => 'User', 'User' => $user, 'token' => $token], Response::HTTP_OK);

            } else {

                return response()->json(['Wrong password!'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {

            return response()->json(['Email doesnt match!'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    public function user_info(Request $request)
    {
        $user = User::find($request->user('sanctum')->id);
        return response()->json(['User info' => $user], Response::HTTP_OK);
    }

    public function deleteAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'phone_number' => ['sometimes', 'required', 'integer', Rule::exists('users', 'phone_number')],
            'email' => ['required', 'email', 'string', 'max:255', Rule::exists('users', 'email')],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user = (new \App\Models\User)->where('email', '=', $request->email)->first();

        if (isset($user->email)) {
            //dd($user);
            $user->delete();
            return response()->json(['Your account has been deleted!'], Response::HTTP_OK);
        }
        return response()->json(['Something Went Wrong!'], Response::HTTP_BAD_REQUEST);
    }
}
