<?php

namespace App\Http\Controllers;

//use App\Events\MyEvent;
use App\Models\Appointment;
use App\Models\Lab;
use App\Models\TestCategory;

use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\TestResult;
use App\Models\Biomarker;
use App\Models\Test;
use App\Models\Range;
use App\Models\User;


class TestResultController extends Controller
{
    public function index()
    {
        $testResult = TestResult::all();
        return response()->json($testResult, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'appointment_id' => 'required|exists:appointments,id',
            'user_id' => 'required|exists:users,id',
            'test_id' => 'required|exists:tests,id',
            'biomarker_id' => 'required|exists:biomarkers,id',
            'result' => 'required',
            //'status' => 'required|string',
            // 'ranges_id' => 'required|exists:ranges,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $test_id = $request->input('test_id');
        $testResult = new TestResult();
        $testResult->test_id = $test_id;
        $testResult->appointment_id = $request->appointment_id;
        $testResult->user_id = $request->user_id;
        $testResult->result = $request->result;
        if ($testResult->result == 'positive') {
            $testResult->status = 'Normal';
        } elseif ($testResult->result == 'negative') {
            $testResult->status = 'Not normal';
        }
        $testResult->biomarker_id = $request->biomarker_id;
        $biomarker = Biomarker::find($testResult->biomarker_id);
        if ($testResult->test_id != $biomarker->test_id) {
            return response()->json(['message' => 'The test you entered dons`t contain this biomarker ']);
        }
        // $appointment = Appointment_Test::find($testResult->test_id);
        ///  if ($testResult->test_id != $appointment->test_id){
        //      return response()->json(['message' => 'The test you intered dosent exists in this appointment ']);
        //  }
        $user = User::find($testResult->user_id);
        $ranges = Range::where('biomarker_id', $testResult->biomarker_id)
            ->where('gender', $user->gender)
            ->where('age_min', '<=', $user->age)
            ->where('age_max', '>=', $user->age)
            ->get();
        foreach ($ranges as $range) {
            if ($testResult->result <= $range->max_value && $testResult->result >= $range->min_value) {
                $testResult->status = 'Normal';
            } else {
                $testResult->status = 'Not normal';
            }
        }
        $testResult->test_date = Carbon::now();
        $testResult->save();
        return response()->json($testResult, Response::HTTP_OK);
    }

    public function store_bio(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'appointment_id' => 'required|exists:appointments,id',
            'user_id' => 'required|exists:users,id',
            'test_id' => 'required|exists:tests,id',
            'biomarker_id' => 'required|exists:biomarkers,id',
            'result' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $test_id = $request->input('test_id');
        $testResult = new TestResult();
        $testResult->test_id = $test_id;
        $testResult->appointment_id = $request->appointment_id;
        $testResult->user_id = $request->user_id;
        $testResult->result = $request->result;
        $testResult->biomarker_id = $request->biomarker_id;

        $biomarker = Biomarker::find($testResult->biomarker_id);
        if ($testResult->test_id != $biomarker->test_id) {
            return response()->json(['message' => 'The test you entered dons`t contain this biomarker !']);
        }

        $biomarker_id = $request->biomarker_id;
        $result = $request->result;
        $user = User::find($testResult->user_id);
        $birthdate = Carbon::parse($user->birth_date);
        $age = $birthdate->diffInYears(Carbon::now());

        if ($biomarker_id) {
            $biomarker = Biomarker::query()->find($biomarker_id);
            $bio_ranges = $biomarker->ranges()->get();
            $inRange = false;
            foreach ($bio_ranges as $range) {
                if ($age >= $range->age_min && $age <= $range->age_max) {
                    $inRange = true;
                    $b_range = $range;
                    break;
                }
            }
            if ($inRange) {
                if ($result >= $b_range->min_value && $result <= $b_range->max_value) {
                    $testResult->status = 'Normal';
                } else if ($result > $b_range->max_value) {
                    $testResult->status = 'High';
                } else {
                    $testResult->status = 'Low';
                }
            }

        }


        $testResult->range_id = $b_range->id;
        $testResult->test_date = Carbon::now();
        $testResult->save();
        return response()->json($testResult, Response::HTTP_OK);
    }

    public function show(Request $request)
    {
        $user_id = $request->user_id;
        $test_id = $request->test_id;
        $testResults = TestResult::with('biomarker')
            ->where('test_id', $test_id)
            ->where('user_id', $user_id)
            ->get();
//        $lab_name = Lab::find(Appointment::find($testResults[0]->appointment_id)->lab_id)->name;
//        $lab_stamp = Lab::find(Appointment::find($testResults[0]->appointment_id)->lab_id)->stamp;
        $lab = Lab::find(Appointment::find($testResults[0]->appointment_id)->lab_id);
        //dd($lab);
        $user = User::find($testResults[0]->user_id);
        $user_age = $user->age;
        $full_name = $user->first_name . ' ' . $user->last_name;
        $age = Carbon::parse($user->birth_date)->diff(Carbon::now())->y;
        $gender = $user->gender;
        //$bio_names = [];
        $bio_mu = [];
        $ranges = [];
        $m_values = [];
        $vv = [];
        foreach ($testResults as $testResult) {
            $biomarker_id = $testResult->biomarker_id;
            // $bio_names [] = Biomarker::find($biomarker_id)->name;
            $bio_mu [] = Biomarker::find($biomarker_id)->measuring_unit;

            $ranges [] = DB::table('ranges')
                ->where('biomarker_id', $biomarker_id)
                ->where('age_min', '<=', $age)
                ->where('age_max', '>=', $age)
                ->get();

        }

//        dd($ranges);
        $test = Test::find($test_id);
        $test_name = $test->name;
        $test_cat_name = TestCategory::find($test->test_category_id)->name;

        if (!$testResults) {
            return response()->json(['message' => 'Test Result not found'], 404);
        }
        //dd($lab->stamp);
        return view('result', ['testResult' => $testResults, 'test_cat_name' => $test_cat_name,
            'bio_mu' => $bio_mu,
            'lab_stamp' => $lab->stamp,
            'ranges' => $ranges,
            'test_name' => $test_name,
            'lab_name' => $lab->name, 'full_name' => $full_name
            , 'age' => $age, 'gender' => $gender
        ]);
    }

    public function saveToPDF(Request $request)
    {
        $user_id = $request->user_id;
        $test_id = $request->test_id;
        $testResults = TestResult::with('biomarker')
            ->where('test_id', $test_id)
            ->where('user_id', $user_id)
            ->get();
        $lab = Lab::find(Appointment::find($testResults[0]->appointment_id)->lab_id);
        $user = User::find($testResults[0]->user_id);
        $full_name = $user->first_name . ' ' . $user->last_name;
        $age = Carbon::parse($user->birth_date)->diff(Carbon::now())->y;
        $gender = $user->gender;
        //$bio_names = [];
        $ranges = [];
        $bio_mu = [];
        foreach ($testResults as $testResult) {
            $biomarker_id = $testResult->biomarker_id;
            // $bio_names [] = Biomarker::find($biomarker_id)->name;
            $bio_mu [] = Biomarker::find($biomarker_id)->measuring_unit;

            $ranges [] = DB::table('ranges')
                ->where('biomarker_id', $biomarker_id)
                ->where('age_min', '<=', $age)
                ->where('age_max', '>=', $age)
                ->get();

        }
        $test = Test::find($test_id);
        $test_name = $test->name;
        $test_cat_name = TestCategory::find($test->test_category_id)->name;

        if (!$testResults) {
            return response()->json(['message' => 'Test Result not found'], 404);
        }
        $pdf = PDF::loadView('resultToPDF', ['testResult' => $testResults, 'test_cat_name' => $test_cat_name,
            'test_name' => $test_name, 'ranges' => $ranges,
            'bio_mu' => $bio_mu, 'lab_stamp' => $lab->stamp,
            //'bio_names' => $bio_names,
            'lab_name' => $lab->name, 'full_name' => $full_name
            , 'age' => $age, 'gender' => $gender
        ]);
        return $pdf->download('results.pdf');
    }

    public function ShowTestResultByUser($userId)
    {
        $testResults = TestResult::where('user_id', $userId)->get();
        if (!$testResults) {
            return response()->json(['message' => 'Test Result not found'], 404);
        }

        return response()->json($testResults, 200);
    }

    public function ShowTestResultByLab($labId)
    {
        //$appointment = Appointment::find($testResult->biomarker_id);
        $testResults = TestResult::where('appointment_id', $labId)->get();
        if (!$testResults) {
            return response()->json(['message' => 'Test Result not found'], 404);
        }

        return response()->json($testResults, 200);
    }

    public function destroy(Request $request)
    {
        $id = $request->query('result_id');
        $testResult = TestResult::find($id);
        if ($request->user('sanctum')->id != $testResult->user_id) {
            return response()->json(['message' => 'ERROR :Your not the owner of this result!'], Response::HTTP_OK);
        }
        $testResult->delete();
        return response()->json(['message' => 'Result deleted successfully'], Response::HTTP_OK);
    }
}
