<!DOCTYPE html>
<html>

<head>
    <title>Medical Lab Test Results</title>

    <style>
        table,
        th,
        td {
            border: 2px solid rgb(92, 174, 188);
            border-collapse: separate;
            border-spacing: 3px;
            border-radius: 10px;
        }

        th,
        td {
            padding: 8px;
            text-align: left;
        }

        #textfield {
            position: absolute;
            top: 10px;
            right: 10px;
        }
        #lab-sign {
            position: absolute;
            bottom: 170px;
            left: 60px;
        }
        #lab-stamp {
            position: absolute;
            bottom: 40px;
            left: 50px;
            width: 100px;
            height: 100px;
        }
    </style>
</head>

<body>

<h2>Medical Lab Test Results</h2>
<label for="text">Test Category: {{$test_cat_name}}</label><br>
<label for="text">Test Type: {{$test_name}}</label><br>
<div id="textfield">
    <label for="text">Lab Name: {{$lab_name}}</label><br>
    <label for="text">Patient Name: {{$full_name}}</label><br>
    <label for="text">Age: {{$age}} years</label><br>
    <label for="text">Gender: {{$gender}}</label><br>
    <label for="text">Test Date: {{ $testResult[0]->test_date }}</label><br>

</div>
<br>
<br>
<br>





<table style="width:100%">
    <tr>
        <th>Test Name</th>
        <th>Result</th>
        <th>Range</th> <!-- Added Range column -->
        <th>Status</th>
    </tr>

    @foreach ($testResult as $i => $result)

        <tr>
            <td>{{ $result->biomarker->name }}</td>
            <td>{{ $result->result }}</td>
            <td>{{$ranges[$i]->first()->min_value }} - {{$ranges[$i]->first()->max_value}} {{ $bio_mu[$i]}}  </td>

            <td style="color: {{ $result->status === 'Low' || $result->status === 'High' ? 'red' : ($result->status === 'Normal' ? 'green' : 'black') }}">{{ $result->status }}</td>
        </tr>
    @endforeach
</table>
<div id="lab-sign">Lab Sign</div>
<img id="lab-stamp" src="{{ $lab_stamp }}" alt="Lab Stamp">
</body>

</html>
