<?php

use App\Models\User;
use App\Notifications\UserTestResult;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/dashboard', function () {
    return view('admin.dashboard');
});


//
//Route::get('push-notification', [NotificationController::class, 'index']);
//Route::post('sendNotification', [NotificationController::class, 'sendNotification'])->name('send.notification');

