<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('auth')->group(function () {
    Route::post('register', [\App\Http\Controllers\Auth\UserController::class, 'register']);
    Route::post('login', [\App\Http\Controllers\Auth\UserController::class, 'login']);
    Route::post('delete', [\App\Http\Controllers\Auth\UserController::class, 'deleteAccount']);
    Route::post('user_info', [\App\Http\Controllers\Auth\UserController::class, 'user_info']);

});

Route::prefix('address')->group(function () {
    Route::get('all_addresses', [\App\Http\Controllers\Address\AddressController::class, 'index']);
    Route::post('store_address', [\App\Http\Controllers\Address\AddressController::class, 'store'])->middleware('user');
    Route::get('show_address', [\App\Http\Controllers\Address\AddressController::class, 'show'])->middleware('user');
    Route::delete('delete_address', [\App\Http\Controllers\Address\AddressController::class, 'destroy'])->middleware('user');
    Route::get('all_cities', [\App\Http\Controllers\Address\CityController::class, 'index']);
    Route::post('store_city', [\App\Http\Controllers\Address\CityController::class, 'store'])->middleware('sysAdmin');
    Route::get('show_city', [\App\Http\Controllers\Address\CityController::class, 'show']);
});

Route::prefix('test_category')->group(function () {
    Route::get('all_test_category', [\App\Http\Controllers\Admin\TestCategoryController::class, 'index']);
    Route::post('add_test_category', [\App\Http\Controllers\Admin\TestCategoryController::class, 'store'])->middleware('sysAdmin');
    Route::put('edit_test_category', [\App\Http\Controllers\Admin\TestCategoryController::class, 'update'])->middleware('sysAdmin');
    Route::get('show_test_category', [\App\Http\Controllers\Admin\TestCategoryController::class, 'show']);
    Route::delete('delete_test_category', [\App\Http\Controllers\Admin\TestCategoryController::class, 'destroy'])->middleware('sysAdmin');
});

Route::prefix('tests')->group(function () {
    Route::get('all_tests', [\App\Http\Controllers\Admin\TestController::class, 'index']);
    Route::post('add_test', [\App\Http\Controllers\Admin\TestController::class, 'store'])->middleware('sysAdmin');
    Route::post('edit_test', [\App\Http\Controllers\Admin\TestController::class, 'update'])->middleware('sysAdmin');
    Route::get('show_test', [\App\Http\Controllers\Admin\TestController::class, 'show']);
    Route::delete('delete_test', [\App\Http\Controllers\Admin\TestController::class, 'destroy'])->middleware('sysAdmin');

    Route::get('lab_tests', [\App\Http\Controllers\Admin\TestController::class, 'lab_tests']);
});

Route::prefix('biomarkers')->group(function () {
    Route::get('all_biomarkers', [\App\Http\Controllers\Admin\BiomarkerController::class, 'index']);
    Route::post('add_biomarker', [\App\Http\Controllers\Admin\BiomarkerController::class, 'store'])->middleware('sysAdmin');
    Route::put('edit_biomarker', [\App\Http\Controllers\Admin\BiomarkerController::class, 'update'])->middleware('sysAdmin');
    Route::get('show_biomarker', [\App\Http\Controllers\Admin\BiomarkerController::class, 'show']);
    Route::delete('delete_biomarker', [\App\Http\Controllers\Admin\BiomarkerController::class, 'destroy'])->middleware('sysAdmin');
    Route::post('statistics_pos', [\App\Http\Controllers\Admin\BiomarkerController::class, 'statistics_pos'])->middleware('sysAdmin');
    Route::post('statistics_low_bio', [\App\Http\Controllers\Admin\BiomarkerController::class, 'statistics_low_bio'])->middleware('sysAdmin');
});

Route::prefix('ranges')->group(function () {
    Route::get('all_ranges', [\App\Http\Controllers\Admin\RangeController::class, 'index'])->middleware('sysAdmin');
    Route::post('add_range', [\App\Http\Controllers\Admin\RangeController::class, 'store'])->middleware('sysAdmin');
    Route::put('edit_range', [\App\Http\Controllers\Admin\RangeController::class, 'update'])->middleware('sysAdmin');
    Route::get('show_range', [\App\Http\Controllers\Admin\RangeController::class, 'show'])->middleware('sysAdmin');
    Route::delete('delete_range', [\App\Http\Controllers\Admin\RangeController::class, 'destroy'])->middleware('sysAdmin');
});

Route::prefix('labs')->group(function () {
    Route::get('all_labs', [\App\Http\Controllers\LabController::class, 'index']);
    Route::post('add_lab', [\App\Http\Controllers\LabController::class, 'store'])->middleware('sysAdmin');
    Route::post('edit_lab', [\App\Http\Controllers\LabController::class, 'update'])->middleware('lab');
    Route::get('show_lab', [\App\Http\Controllers\LabController::class, 'show']);
    Route::delete('delete_lab', [\App\Http\Controllers\LabController::class, 'destroy'])->middleware('sysAdmin');
    Route::get('nearby_labs', [\App\Http\Controllers\LabController::class, 'nearby_labs']);
    Route::get('top_rated_labs', [\App\Http\Controllers\LabController::class, 'top_rated_labs']);
});

Route::prefix('rates')->group(function () {
    Route::post('add', [\App\Http\Controllers\RateController::class, 'store']);
    Route::post('edit', [\App\Http\Controllers\RateController::class, 'update']);
    Route::get('show', [\App\Http\Controllers\RateController::class, 'show']);
    Route::delete('delete', [\App\Http\Controllers\RateController::class, 'destroy']);
});

Route::prefix('medical_articles')->group(function () {
    Route::get('all_articles', [\App\Http\Controllers\MedicalArticlesController::class, 'index']);
    Route::post('add_article', [\App\Http\Controllers\MedicalArticlesController::class, 'store'])->middleware('sysAdmin');
    Route::put('edit_article', [\App\Http\Controllers\MedicalArticlesController::class, 'update'])->middleware('sysAdmin');
    Route::get('show_article', [\App\Http\Controllers\MedicalArticlesController::class, 'show']);
    Route::delete('delete_article', [\App\Http\Controllers\MedicalArticlesController::class, 'destroy'])->middleware('sysAdmin');

});

Route::prefix('appInfo')->group(function () {
    Route::get('show_appInfo', [\App\Http\Controllers\AppInfoController::class, 'show_appInfo']);
    Route::post('add_appInfo', [\App\Http\Controllers\AppInfoController::class, 'add_appInfo']);
    Route::put('edit_appInfo/{id}', [\App\Http\Controllers\AppInfoController::class, 'edit_appInfo']);
});

Route::prefix('appointments')->group(function () {
    Route::get('index', [\App\Http\Controllers\AppointmentController::class, 'index'])->middleware('sysAdmin');
    Route::post('store', [\App\Http\Controllers\AppointmentController::class, 'store'])->middleware('user');
    Route::get('show_appointment_by_UserId', [\App\Http\Controllers\AppointmentController::class, 'show_appointment_by_UserId'])->middleware('user');
    Route::get('show_appointment_by_LabId', [\App\Http\Controllers\AppointmentController::class, 'show_appointment_by_LabId'])->middleware('lab');
    Route::delete('delete_appointment', [\App\Http\Controllers\AppointmentController::class, 'destroy'])->middleware('user');
    Route::get('done_appointments', [\App\Http\Controllers\AppointmentController::class, 'done_appointments']);
    Route::get('pending_appointments', [\App\Http\Controllers\AppointmentController::class, 'pending_appointments']);
});

Route::prefix('results')->group(function () {
    Route::get('all_results', [\App\Http\Controllers\TestResultController::class, 'index'])->middleware('sysAdmin');
    Route::post('add_result', [\App\Http\Controllers\TestResultController::class, 'store'])->middleware('lab');
    Route::post('add_result_bio', [\App\Http\Controllers\TestResultController::class, 'store_bio'])->middleware('lab');
    Route::get('show_result', [\App\Http\Controllers\TestResultController::class, 'show']);
    //->middleware('user');
    Route::get('save_to_pdf', [\App\Http\Controllers\TestResultController::class, 'saveToPDF']);
    //->middleware('user');
    Route::delete('delete_result', [\App\Http\Controllers\TestResultController::class, 'destroy'])->middleware('user');
});

Route::prefix('medicalArticles')->group(function () {
    Route::get('show_all_medicalArticles', [\App\Http\Controllers\MedicalArticlesController::class, 'index']);
    Route::post('add_medicalArticles', [\App\Http\Controllers\MedicalArticlesController::class, 'store']);
    Route::put('edit_medicalArticles/{id}', [\App\Http\Controllers\MedicalArticlesController::class, 'update']);
    Route::get('show_medicalArticles_by_id/{id}', [\App\Http\Controllers\MedicalArticlesController::class, 'show']);
    Route::delete('delete_medicalArticles/{id}', [\App\Http\Controllers\MedicalArticlesController::class, 'destroy']);
});

